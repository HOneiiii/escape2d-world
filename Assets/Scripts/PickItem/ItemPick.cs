using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPick : MonoBehaviour
{

    [Header("放入当前物体数据文件")]
    public PickItem_SO item;
    [Header("背包数据文件")]
    public BagData_SO bagData;

    private Transform playerTransform;
    private Transform startTransform;
    public bool isPicked;
    public bool allowPick;

    private void OnEnable() //初始化
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
        startTransform = this.transform;
        isPicked = false;
    }

    // Update is called once per frame
    void Update()
    {
        Pick();
    }

    private void OnTriggerEnter2D(Collider2D collision) //判断是否可以开始拾取
    {
        IsEmpty();

        if (collision.gameObject.tag == "Player" && allowPick)
        {
                isPicked = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) 
    {
        if (other.gameObject.tag == "Player")
        {
                isPicked = false;
        }
    }

    private void IsEmpty() //判断背包是否已满
    {
        if (bagData.itemList.Count >= 6)
        {
            for (int i = 0; i < bagData.itemList.Count; i++)
            {
                if (item.itemName == bagData.itemList[i].itemName)
                {
                    allowPick = true;
                }
                if (bagData.itemList[i] == null)
                {
                    allowPick = true;
                }
                else
                {
                    allowPick = false;
                }
            }
        }
        else
        {
            allowPick = true;
        }
    }

    private void Pick() //拾取动作
    {
        if (isPicked && Input.GetKeyDown(KeyCode.E))
        {
                Destroy(this.gameObject);
                AddToBag();

                ItemPickEvent.Trigger(); //发送拾取事件 刷新背包UI
        }
    }

    private void AddToBag() //更新背包数据
    {
        if (bagData.itemList.Contains(item)) //有相同的增加数量
        {
            item.itemNum += 1;
        }
        else
        {
            for (int i = 0; i < bagData.itemList.Count; i++) //遍历 若有空位插入空位 若无 插入list最后
            {
                if(bagData.itemList[i] == null)
                {
                    bagData.itemList[i] = item;
                    item.itemNum = 1;
                    return;
                }
            }
            bagData.itemList.Add(item);
            item.itemNum = 1;
        }
    }
}
