using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncinessBlock : MonoBehaviour
{
    public Vector2 bounForce;
    
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player")
            other.gameObject.GetComponent<Rigidbody2D>().AddForce(bounForce);
    }

    private void OnTriggerExit2D(Collider2D other) {
        
    }
}
