using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TalkCheck : MonoBehaviour
{
    public GameObject npc;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (npc.GetComponent<NPC_Talk>().allowTalk)
        {
            npc.GetComponent<NPC_Talk>().canTalk = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        npc.GetComponent<NPC_Talk>().canTalk = false;
    }
}
