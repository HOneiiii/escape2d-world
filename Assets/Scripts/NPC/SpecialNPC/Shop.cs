using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject meat;

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.GetComponent<ItemPick>().item.itemName == "Coin")
        {
            Destroy(other.gameObject);
            Instantiate(meat,this.transform.position,this.transform.rotation,GameObject.Find("PickItem").transform);
        }
    }
}
