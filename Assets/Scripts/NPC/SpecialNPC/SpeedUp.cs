using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUp : MonoBehaviour
{
    private void OnMouseDown() {
        GameObject.Find("Player").GetComponent<PlayerMove>().moveSpeed = 8;
        GameObject.Find("Player").GetComponent<PlayerMove>().maxJumpTimes = 1;
        GameObject.Find("Player").GetComponent<PlayerMove>().jumpTimes = 1;
    }
}
