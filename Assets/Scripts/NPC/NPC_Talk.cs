//插件使用指南
//
//[公共参数]allowTalk
//NPC是否开启对话功能
//txt文本最后需要加一行end.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC_Talk : MonoBehaviour
{
    //公共参数
    [Header("NPC姓名")]
    public string npcName;
    [Header("是否是可对话NPC")]
    public bool allowTalk;
    [Header("是否循环对话")]
    public bool isLoop;
    [Header("对话文本")]
    public TextAsset[] talkTxt;
    [Header("对话提示")]
    public GameObject talkSign;

    //内部参数
    [HideInInspector] public bool canTalk;
    private int txtOrder; //文本指针
    private GameObject player;
    private GameObject text;
    private int textRow;
    private bool isTalking;

    public enum State
    {
        Idle,
        Talking,
        ReadyToTalk
    }

    public State npc_State;

    void Start()
    {
        canTalk = false;
        textRow = 0;

        player = GameObject.Find("Player");
    }


    void Update()
    {
        ShowSign();
        showText();
        CleanData();
        StateControl();
    }

    private void ShowSign() //生成头顶标识
    {
        if (canTalk)
        {
            this.talkSign.SetActive(true);
        }
        else
        {
            this.talkSign.SetActive(false);
        }
    }

    private void OnMouseDown() //点击NPC显示对话UI 并重置Txt文本读取位置
    {
        if (canTalk)
        {
            isTalking = true;

            GameObject canvas = GameObject.Find("Canvas");
            Transform panel = canvas.transform.Find("NPCTalk_Panel");
            panel.gameObject.SetActive(true);
            textRow = 0;
        }
    }

    private void showText() //链接txt文本与UI界面Text 并且逐行读取显示 读取完毕隐藏UI
    {
        GameObject canvas = GameObject.Find("Canvas");
        Transform panel = canvas.transform.Find("NPCTalk_Panel");
        Text text = canvas.transform.Find("NPCTalk_Panel/NPCWord").gameObject.GetComponent<Text>();

        string[] str = talkTxt[txtOrder].text.Split('\n');

        if (Input.GetMouseButtonDown(0) && isTalking)
        {
            canvas.transform.Find("NPCTalk_Panel/NPCName").gameObject.GetComponent<Text>().text = npcName;
            canvas.transform.Find("NPCTalk_Panel/Sprite").gameObject.GetComponent<Image>().sprite = this.GetComponent<SpriteRenderer>().sprite;
            text.text = str[textRow];
            textRow = textRow + 1;
        }

        if (textRow == str.Length)
        {
            panel.gameObject.SetActive(false);

            textRow = 0;
            txtOrder = txtOrder + 1; //第一个文本播完后 加载第二个文本
            if (txtOrder == talkTxt.Length)
            {
                txtOrder = 0; //全部文本播完后 重置文本指针
                if (!isLoop) //如果为不循环播放 则变为不可Talk的NPC
                {
                    allowTalk = false;
                    canTalk = false;
                }
            }
            isTalking = false;
        }
    }

    private void CleanData()    //走出对话区域重置当前文本
    {
        if (!canTalk && isTalking)
        {
            GameObject canvas = GameObject.Find("Canvas");
            Transform panel = canvas.transform.Find("NPCTalk_Panel");

            textRow = 0;
            isTalking = false;
            panel.gameObject.SetActive(false);
        }
    }

    private void StateControl()
    {
        if (!canTalk)
        {
            npc_State = State.Idle;
        }
        if (canTalk && !isTalking)
        {
            npc_State = State.ReadyToTalk;
        }
        if (isTalking)
        {
            npc_State = State.Talking;
        }
    }
}
