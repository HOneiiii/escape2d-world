using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameState
{
    public enum State
    {
        Ready,
        Gaming,
        End
    }

    public static State state;
}
