using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX_Active : MonoBehaviour
{
    public void DisableFX() //动画结束 隐藏FX
    {
        this.gameObject.SetActive(false);
    }
}
