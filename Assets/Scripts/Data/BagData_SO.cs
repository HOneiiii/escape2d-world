using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bag_Data",menuName ="BagSystem/BagData")]
public class BagData_SO : ScriptableObject
{
    public List<PickItem_SO> itemList = new List<PickItem_SO>();
}
