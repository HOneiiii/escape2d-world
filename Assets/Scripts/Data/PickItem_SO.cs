using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PickItem_Data",menuName ="BagSystem/PickItem")]
public class PickItem_SO : ScriptableObject
{
    public string itemName;
    public int itemNum;
    public Sprite sprite;
    public string itemInfo;
}