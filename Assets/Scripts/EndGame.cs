using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    public GameObject End_Panel;

    private void OnTriggerEnter2D(Collider2D other)
    {
        GameState.state = GameState.State.End;
        End_Panel.SetActive(true);
    }
}
