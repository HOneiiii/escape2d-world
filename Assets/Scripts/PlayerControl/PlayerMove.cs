using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerMove : MonoBehaviour
{
    //公共参数
    [Header("移动/跳跃")]
    [Range(0, 20)] [Tooltip("移动速度")] public float moveSpeed;
    [Range(0, 50)] [Tooltip("跳跃高度")] public float JumpForce;
    [Range(0, 5)] [Tooltip("X段跳")] public int maxJumpTimes;
    [Tooltip("冲刺开关")]public bool allowRush;
    [Range(0, 100)] [Tooltip("冲刺距离")] public float rushForce;
    [Range(0, 50)] [Tooltip("冲刺结束高度补偿")] public float rushEndJump; //冲刺补偿
    [Range(-180, 180)] [Tooltip("武器最大角度")]public float maxAngle;
    [Range(-180, 180)] [Tooltip("武器最小角度")]public float minAngle;
    [Header("特效")]
    [Tooltip("瞬移特效 将带有Animator的GameObject放入此空位")] public GameObject FX_Disapear;
    [Tooltip("落地特效 将带有Animator的GameObject放入此空位")] public GameObject FX_Dust_Land;

    //内部参数
    private Transform weaponHolder;
    private Animator anim; //动画机
    private Rigidbody2D rb; //Player刚体
    private SpriteRenderer spr; //Player精灵
    [HideInInspector] public int jumpTimes; //跳跃次数
    private bool canRush; //可否冲刺
    private bool isGrounded;
    private PickItem_SO holdItem;

    void Start()
    {
        anim = GetComponent<Animator>(); //获取动画机
        rb = GetComponent<Rigidbody2D>(); //获取刚体
        spr = GetComponent<SpriteRenderer>(); //获取精灵

        weaponHolder = transform.Find("WeaponHolder"); //获取武器持有点

        //初始化常量
        jumpTimes = 0;
        canRush = false;
        isGrounded = false;

        //注册事件
        JumpCheckEvent.Register(isLanded);
        JumpLeaveEvent.Register(isJumping);
        JumpCheckEvent.Register(FX_Dust_LandPlay);
        DropItemEvent.Register(DropItem);

        GetComponent<BagSystem>().swtichHoldItem += ChangeHold;
    }

    void Update()
    {
        Jump();
        FX_DisapearPlay();
        Rush();
        WeaponAngle();
        ChangeAnimator();
    }

    private void LateUpdate()
    {
        if (Input.GetMouseButtonDown(1))
        {
            canRush = false;
        }
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move() //移动逻辑
    {
        float Input_X = Input.GetAxisRaw("Horizontal");

        rb.velocity = new Vector2(Input_X * moveSpeed, rb.velocity.y);
    }

    private void Jump() //跳跃逻辑
    {
        if (jumpTimes > 0 && Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = new Vector2(rb.velocity.x, JumpForce);
            anim.SetTrigger("Jump");    //动画播放
            jumpTimes = jumpTimes - 1; //二段跳计数
        }
    }

    private void Rush()
    {
        if (canRush && Input.GetMouseButtonDown(1))
        {
            //获取鼠标位置
            Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
　　        Vector3 mousePosOnScreen = Input.mousePosition;
　　        mousePosOnScreen.z = screenPos.z;
　　        Vector3 mousePosInWorld = Camera.main.ScreenToWorldPoint(mousePosOnScreen);

            //发射一条player到鼠标位置的射线 并返回射线碰撞信息
            RaycastHit2D wall = Physics2D.Raycast(this.transform.position,(mousePosInWorld - this.transform.position).normalized);

            //判断是否撞墙 撞墙则停止在墙前
            if(wall == true && (wall.point - new Vector2(this.transform.position.x,this.transform.position.y)).sqrMagnitude < new Vector2(mousePosInWorld.x - this.transform.position.x,mousePosInWorld.y - this.transform.position.y).sqrMagnitude)
            {
                mousePosInWorld = wall.point -(wall.point - new Vector2(this.transform.position.x,this.transform.position.y)).normalized * 0.8f; //0.8f为防卡墙参数
            }

            //若不撞墙 判断最大可瞬移距离
            if ((mousePosInWorld - this.transform.position).sqrMagnitude < ((mousePosInWorld - this.transform.position).normalized * rushForce).sqrMagnitude)
            {
                this.transform.position = mousePosInWorld;
            }else
            {
                this.transform.position += (mousePosInWorld - this.transform.position).normalized * rushForce;
            }
            
            //跳跃补偿
            rb.velocity = new Vector2(rb.velocity.x, rushEndJump);
        }
    }

    private void isLanded() //落地事件 监听JumpCheck的Trigger
    {
        isGrounded = true;
        jumpTimes = maxJumpTimes;
        canRush = false;
    }

    private void isJumping() //起跳事件 监听JumpCheck的Trigger
    {
        isGrounded = false;
        if(allowRush)
        {
            canRush = true;
        }
    }

    private void FX_DisapearPlay() //空中冲刺的动画
    {
        if (!isGrounded && Input.GetMouseButtonDown(1) && canRush)
        {
            FX_Disapear.SetActive(true);
            FX_Disapear.transform.position = this.transform.position;
        }
    }

    private void FX_Dust_LandPlay() //落地的动画 监听JumpCheck的落地Trigger
    {
        FX_Dust_Land.SetActive(true);
        FX_Dust_Land.transform.position = this.transform.position;
    }

    private void ChangeAnimator() //改变朝向以及动画
    {
        anim.SetFloat("speed", rb.velocity.sqrMagnitude);
        if (rb.velocity.x > 0.1)
        {
            spr.flipX = false;
        }
        else if (rb.velocity.x < -0.1)
        {
            spr.flipX = true;
        }
    }

    private void WeaponAngle()
    {
        //获取鼠标位置
        Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
　　     Vector3 mousePosOnScreen = Input.mousePosition;
　　     mousePosOnScreen.z = screenPos.z;
　　     Vector3 mousePosInWorld = Camera.main.ScreenToWorldPoint(mousePosOnScreen);

        Vector2 direction = mousePosInWorld - weaponHolder.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        if(angle > maxAngle)
        {
            angle = maxAngle;
        }if(angle <minAngle)
        {
            angle = minAngle;
        }

        weaponHolder.transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }

    private void ChangeHold(PickItem_SO item)
    {
        if(item == null)
        {
            weaponHolder.GetComponent<SpriteRenderer>().sprite = null;
            holdItem = null;
        }
        else
        {
            weaponHolder.GetComponent<SpriteRenderer>().sprite = item.sprite;
            holdItem = item;
        }
    }

    public void DropItem()
    {
        if(GetComponent<BagSystem>().bagData.itemList[GetComponent<BagSystem>().listOrder-1] == null) //现在持有物体的背包数据
        {
            weaponHolder.GetComponent<SpriteRenderer>().sprite = null;
        }
    }

    private void OnDestroy() //注销事件
    {
        JumpCheckEvent.Unregister(isLanded);
        JumpLeaveEvent.Unregister(isJumping);
        JumpCheckEvent.Unregister(FX_Dust_LandPlay);
        DropItemEvent.Unregister(DropItem);
        
        GetComponent<BagSystem>().swtichHoldItem -= ChangeHold;
    }
}
