using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BagSystem : MonoBehaviour
{
    [Header("背包物品模板")]
    public GameObject bagIcon;
    [Header("背包数据文件")]
    public BagData_SO bagData;
    [Header("背包显示界面")]
    public GameObject item_Panel;
    public PickItem_SO holdItem;
    public GameObject dropItem;

    [HideInInspector] public int listOrder;
    private RectTransform bagTag;
    List<PickItem_SO> itemList = new List<PickItem_SO>();

    public delegate void SwitchHoldItem(PickItem_SO pickItem_SO);
    public SwitchHoldItem swtichHoldItem;

    void Start()
    {
        itemList = bagData.itemList;
        listOrder = 1;
        holdItem = null;


        bagTag = GameObject.Find("BagTag").GetComponent<RectTransform>();

        ItemPickEvent.Register(UpdateBagUI);
        UpDateBagEvent.Register(UpdateBagUI);
        ItemPickEvent.Register(HoldUpdate);
        UpDateBagEvent.Register(HoldUpdate);

        UpDateBagEvent.Trigger();
    }

    private void Update()
    {
        SwitchHold();
        DropItem();
        EatMeat();
    }

    public void UpdateBagUI() //更新背包
    {
        //删除UI背包中的图像
        for (int i = 0; i < item_Panel.transform.childCount; i++)
        {
            Destroy(item_Panel.transform.GetChild(i).gameObject);
        }

        //绘制UI背包中的图像 单个物品通过更改BagIcon的图像、文本进行显示，本身并没有预制体，通过数据文件+BagIcon进行表现
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i] != null)
            {
                bagIcon.GetComponent<Image>().color = new Color(255, 255, 255, 100);
                bagIcon.transform.Find("Image").GetComponent<Image>().color = new Color(255, 255, 255, 100);
                bagIcon.GetComponent<Image>().sprite = itemList[i].sprite;
                bagIcon.transform.Find("Text").GetComponent<Text>().text = itemList[i].itemNum.ToString();
            }
            else
            {
                bagIcon.GetComponent<Image>().sprite = null;
                bagIcon.GetComponent<Image>().color = new Color(0, 0, 0, 0);
                bagIcon.transform.Find("Text").GetComponent<Text>().text = null;
                bagIcon.transform.Find("Image").GetComponent<Image>().color = new Color(0, 0, 0, 0);
            }
            Instantiate(bagIcon, item_Panel.transform);
        }
    }

    public void HoldUpdate()
    {
        holdItem = bagData.itemList[listOrder - 1];
    }

    public void SwitchHold() //切换手持物 并且显示角标
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            listOrder = 1;
            bagTag.anchoredPosition = new Vector3(-41, 4.7f, 0);
            if (itemList.Count < 1)
            {
                holdItem = null;
            }
            else
            {
                holdItem = itemList[0];
            }
            swtichHoldItem(holdItem);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            listOrder = 2;
            bagTag.anchoredPosition = new Vector3(-24.2f, 4.7f, 0);
            if (itemList[1] != null)
                if (itemList.Count < 2)
                {
                    holdItem = null;
                }
                else
                {
                    holdItem = itemList[1];
                }
            swtichHoldItem(holdItem);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            listOrder = 3;
            bagTag.anchoredPosition = new Vector3(-7.5f, 4.7f, 0);
            if (itemList.Count < 3)
            {
                holdItem = null;
            }
            else
            {
                holdItem = itemList[2];
            }
            swtichHoldItem(holdItem);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            listOrder = 4;
            bagTag.anchoredPosition = new Vector3(9.15f, 4.7f, 0);
            if (itemList.Count < 4)
            {
                holdItem = null;
            }
            else
            {
                holdItem = itemList[3];
            }
            swtichHoldItem(holdItem);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            listOrder = 5;
            bagTag.anchoredPosition = new Vector3(25.8f, 4.7f, 0);
            if (itemList.Count < 5)
            {
                holdItem = null;
            }
            else
            {
                holdItem = itemList[4];
            }
            swtichHoldItem(holdItem);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            listOrder = 6;
            bagTag.anchoredPosition = new Vector3(43.45f, 4.7f, 0);
            if (itemList.Count < 6)
            {
                holdItem = null;
            }
            else
            {
                holdItem = itemList[5];
            }
            swtichHoldItem(holdItem);
        }
    }

    public void DropItem()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            bagData.itemList[listOrder - 1].itemNum--;
            if (bagData.itemList[listOrder - 1].itemNum == 0)
            {
                bagData.itemList[listOrder - 1] = null;
                DropItemCreate();
                DropItemEvent.Trigger();
            }
            UpdateBagUI();
            if (bagData.itemList[listOrder - 1].itemNum > 0)
            {
                DropItemCreate();
            }
        }
    }

    public void DropItemCreate()
    {
        GameObject mItem = Instantiate(dropItem, this.transform.position + new Vector3(0, 1.5f, 0), this.transform.rotation, GameObject.Find("PickItem").transform);
        mItem.GetComponent<SpriteRenderer>().sprite = holdItem.sprite;
        mItem.GetComponent<ItemPick>().item = holdItem;

        float x = UnityEngine.Random.Range(-200, 200);
        float y = UnityEngine.Random.Range(80, 100);
        mItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(x, y));
    }

    private void OnDestroy()
    {
        ItemPickEvent.Unregister(UpdateBagUI);
        UpDateBagEvent.Unregister(UpdateBagUI);
        ItemPickEvent.Unregister(HoldUpdate);
        UpDateBagEvent.Unregister(HoldUpdate);
    }

    private void EatMeat()
    {
        if (Input.GetKeyDown(KeyCode.F) && holdItem.itemName == "Meat")
        {
            if (bagData.itemList[listOrder - 1].itemNum == 1)
            {
                bagData.itemList[listOrder - 1] = null;
                swtichHoldItem(null);
            }
            else
            {
                bagData.itemList[listOrder - 1].itemNum--;
            }
            GetComponent<PlayerMove>().jumpTimes = 3;
            UpdateBagUI();
        }
    }
}
