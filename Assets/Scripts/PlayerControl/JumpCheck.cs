using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCheck : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Floor")
        {
            JumpCheckEvent.Trigger();
        }
        if(other.tag == "DeadFloor")
        {
            Destroy(GameObject.FindWithTag("Player"));
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        JumpLeaveEvent.Trigger();
    }
}
