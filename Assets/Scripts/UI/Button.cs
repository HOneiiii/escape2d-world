using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public BagData_SO Bag_Data;

    public void StartGame()
    {
        SceneManager.LoadScene(1);
        GameState.state = GameState.State.Gaming;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
        GameState.state = GameState.State.Ready;
    }

}
