using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public BagData_SO bagData;
    public GameObject Player;
    public Transform Spawn;
    public GameObject ESC_Panel;
    public GameObject End_Panel;

    private void Awake()
    {
        //Application.targetFrameRate = 144;
    }

    // Start is called before the first frame update
    void Start()
    {
        bagData.itemList.Clear();
        ItemPickEvent.Trigger();
    }

    // Update is called once per frame
    void Update()
    {
        KeyCheck();
    }

    private void KeyCheck()
    {
        if (Input.GetKeyDown(KeyCode.Escape) & GameState.state != GameState.State.End)
        {
            if (ESC_Panel.activeSelf == false)
            {
                ESC_Panel.SetActive(true);
            }
            else
            {
                ESC_Panel.SetActive(false);
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Player.transform.position = Save.savePos.position;
        }
    }
}
