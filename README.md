# Escape2dWorld

## 游戏简介

本游戏为一个非常简单的小游戏

但是构建了如下系统

### 玩家控制系统

1. 移动系统

2. 跳跃系统（包含可自定义的多段跳）

3. 冲刺系统

### NPC系统

1. 对话系统

2. 简单的任务系统

### 背包系统

1. 掉落物系统

2. 简单的交易系统

## 游戏截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/143303_cc796393_8627435.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/143314_2be75730_8627435.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/143325_afa2f8da_8627435.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1120/143334_dffdb1c2_8627435.png "屏幕截图.png")


