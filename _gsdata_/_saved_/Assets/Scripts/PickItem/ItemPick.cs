using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPick : MonoBehaviour
{
    private Transform playerTransform;
    private Transform startTransform;
    public float pickSpeed;
    public float pickDistance;
    private bool isPicked;
    public PickItem_SO item;
    public BagData_SO bagData;

    private void OnEnable()
    {
        playerTransform = GameObject.FindWithTag("Player").transform;
        startTransform = this.transform;
        isPicked = false;
    }

    // Update is called once per frame
    void Update()
    {
        Pick();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            isPicked = true;
        }

    }

    private void Pick()
    {
        if (isPicked)
        {
            this.transform.position = Vector3.Lerp(startTransform.position, playerTransform.position, Time.deltaTime * pickSpeed);
            if((this.transform.position - playerTransform.position).sqrMagnitude < pickDistance )
            {
                AddToBag();
                Destroy(this.gameObject);
            }
        }
    }

    private void AddToBag()
    {
        if(bagData.itemList.Contains(item))
        {
            item.itemNum += 1;
        }
        else
        {
            bagData.itemList.Add(item);
            item.itemNum = 1;
        }
    }
}
