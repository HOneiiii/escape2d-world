using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FX_Active : MonoBehaviour
{
    public void DisableFX()
    {
        this.gameObject.SetActive(false);
    }
}
