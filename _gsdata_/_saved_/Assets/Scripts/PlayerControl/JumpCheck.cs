using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCheck : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        JumpCheckEvent.Trigger();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        JumpLeaveEvent.Trigger();
    }
}
